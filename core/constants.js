const FPS = 60
const SPRITE_SIZE = 16
const LOCAL_STORAGE = "GJL_LOCAL_STORAGE"

// Virtual Screen where game is rendered
let SCREEN 
const GAME_HEIGHT = 176
const GAME_WIDTH = 320

// Canvas where the screen is rendered
let CANVAS
const SCALE = 3
const CANVAS_HEIGHT =  SCALE * GAME_HEIGHT
const CANVAS_WIDTH = SCALE * GAME_WIDTH
const CANVAS_TO_GAME = GAME_HEIGHT / CANVAS_HEIGHT

// Entities
const MAX_ENTITIES = 5000


// Local storage

function loadSavedState() {
    const savedState = window.localStorage.getItem(LOCAL_STORAGE)
    if (savedState) {
        try {
            STATE = JSON.parse(savedState)
        } catch(e) { console.error(e) }
    }
}

function saveState() {
    const STATE_TO_SAVE = Object.assign({}, STATE)
    STATE_TO_SAVE.entities = []
    const stateJSON = JSON.stringify(STATE_TO_SAVE)

    window.localStorage.setItem(LOCAL_STORAGE, stateJSON)
}

// Util
function between(x, a,b) {
    return x >= a && x <= b;
}
function inside(px,py, x,y,w,h) {
    return between(px, x, x+w) && between(py, y, y+h)
}

function stepTo(a, b, x) {
    if (!x) x = 1

    const diff = b - a
    if (Math.abs(diff) <= x) return b
    return a + x * Math.sign(diff)
}

function randomIntBetween(a,b) {
    return a + Math.floor(Math.random() * (b-a))
}

function clamp(x, a,b) {
    return Math.max(a, Math.min(x, b))
}

function squareWave(frequency, duty_cycle) {
    duty_cycle = clamp(duty_cycle, 0,1)
    if (frequency == 0) return 1

    const period = 1 / frequency
    const time = STATE.t / FPS
    const phase = (time % period) / period
    return phase < duty_cycle ? 1 : 0
}

function mapValue(a,b, x) {
    return a + x * (b - a)
}