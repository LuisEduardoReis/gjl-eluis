
let FONT = []
let FONT_BLACK = []
let FONT_RED = []
let FONT_GREEN = []

const fontW = 3
const fontH = 5
const fontS = 1

function parseLine(start, end, y) {
    for (let c = 0; c < end-start; c++) {
        FONT[c + start] = subImage(
            FONT_IMAGE, 
            c * (fontW + fontS), y * fontH,
            fontW+1, fontH
        )        
    }
}

function stringWidth(string) {
    if (typeof string != 'string') string = ''

    let nSpaces = 0
    for (let i = 0; i < string.length; i++) 
        if (string[i] == ' ') nSpaces++

    return (string.length * (fontW + fontS)) - nSpaces
}

function createFont() {
    let char = (c) => c.charCodeAt(0)
    
    FONT[char(' ')] = createImage(fontW, fontH)
    parseLine(char("'"), char('?'), 0)
    parseLine(char("A"), char('Z'), 1)
    parseLine(char("a"), char('z'), 2)


    FONT_BLACK = FONT.map(sprite => tintSprite(sprite, color('black')))
    FONT_RED = FONT.map(sprite => tintSprite(sprite, color('red')))
    FONT_GREEN = FONT.map(sprite => tintSprite(sprite, color(32,255,32)))
}

function drawText(string, x,y, align, noBackground) {
    drawTextFont(string, FONT, x,y,align, noBackground)    
}

function drawTextFont(string, font, x,y, align, noBackground) {
    if (!noBackground) drawTextFont(string, FONT_BLACK, x+1,y+1, align, true)

    if (typeof string == 'number') string += ''
    if (typeof string != 'string') string = ''

    if (align == 'center')
        x -= stringWidth(string) / 2

    string = string.toUpperCase()
    for (let i = 0; i < string.length; i++) {
        drawSprite(font[string.charCodeAt(i)], x,y)
        if (string[i] == ' ') x--
        x += fontW + fontS
    }
    
}