const SPRITES = {}

function createSprites() {

    // SELL BUTTON
    SPRITES.sell = {}
    SPRITES.sell.up = createSprite(0,1, 3,1)
    SPRITES.sell.down = createSprite(0,2, 3,1)
    SPRITES.sell.over = createSprite(0,3, 3,1)

    // UPGRADES
    SPRITES.upgrades = {}
    SPRITES.upgrades.serverUp = tintSprite(createSprite(3,1, 1,1), color(255))
    SPRITES.upgrades.serverOver = tintSprite(createSprite(3,1, 1,1), color(200))
    SPRITES.upgrades.serverDown = tintSprite(createSprite(3,1, 1,1), color(64))

    SPRITES.upgrades.firewallUp = tintSprite(createSprite(3,3, 1,1), color(255))
    SPRITES.upgrades.firewallOver = tintSprite(createSprite(3,3, 1,1), color(200))
    SPRITES.upgrades.firewallDown = tintSprite(createSprite(3,3, 1,1), color(100))

    SPRITES.upgrades.sellUp = tintSprite(createSprite(3,2, 1,1), color(255))
    SPRITES.upgrades.sellOver = tintSprite(createSprite(3,2, 1,1), color(200))
    SPRITES.upgrades.sellDown = tintSprite(createSprite(3,2, 1,1), color(100))
    

    // INFRASTRUCTURE
    SPRITES.infra = {}
    SPRITES.infra.server = createSprite(0,4, 1,2)
    SPRITES.infra.serverRed = createTintedSprite(
        SPRITES.infra.server,
        color('white'), color('red'),
        128
    )

    SPRITES.infra.fireawll = createSprite(2,4, 1,2)
    SPRITES.infra.firewallRed = createTintedSprite(
        SPRITES.infra.fireawll,
        color('white'), color('red'),
        128
    )

    // LE MONKEY
    SPRITES.monkey = {}
    SPRITES.monkey.normal = createTintedSprite(
        createSprite(1,0, 1,1),
        color(255, 255), color(255, 0), 32
    )

    SPRITES.monkey.angry = createTintedSprite(
        createSprite(2,0, 1,1),
        color(255, 255), color(255, 0), 32
    )
    SPRITES.monkey.dead = createTintedSprite(
        createSprite(3,0, 1,1),
        color(255, 255), color(255, 0), 32
    )

    // ITEMS
    SPRITES.items = []
    for (let i = 0; i < 8; i++) {
        SPRITES.items.push(createSprite(i,7, 1,1))
    }
    
}

// Sprite Functions
function subImage(src, x,y,w,h) {
    return src.get(x,y,w,h)
}

function createSprite(x,y, w,h) {
    return subImage(
        SPRITE_SHEET,
        x * SPRITE_SIZE, y * SPRITE_SIZE,
        w * SPRITE_SIZE, h * SPRITE_SIZE
    )
}

function tintSprite(sprite, color) {
    SCREEN.clear()
    SCREEN.tint(color)
    SCREEN.image(sprite,0,0)
    SCREEN.noTint()
    return SCREEN.get(0,0, sprite.width, sprite.height)
}

function createTintedSprite(sprite, from, to, steps) {
    if (!steps) steps = 16
    const result = []
    
    for (let i = 0; i < steps; i++) {
        let r = mapValue(from._getRed(), to._getRed(), i / (steps-1))
        let g = mapValue(from._getGreen(), to._getGreen(), i / (steps-1))
        let b = mapValue(from._getBlue(), to._getBlue(), i / (steps-1))
        let a = mapValue(from._getAlpha(), to._getAlpha(), i / (steps-1))
        let mix = color(r,g,b,a)

        result.push(tintSprite(sprite, mix))
    }

    return result
}

function drawSprite(sprite, x,y) {
    SCREEN.image(sprite, Math.floor(x), Math.floor(y))
}

function drawTintedSprite(spriteList, x,y, v) {
    v = clamp(v, 0,1)
    let i = Math.floor(v * (spriteList.length - 1))
    drawSprite(spriteList[i], x,y)
}