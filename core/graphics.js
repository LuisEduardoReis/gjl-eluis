let SPRITE_SHEET = null
let BACKGROUND = null
let WEBSITE_BACKGROUND = null
let FONT_IMAGE = null

function loadImages() {
    SPRITE_SHEET = loadImage("assets/sprites.png")
    BACKGROUND = loadImage("assets/background.png")
    WEBSITE_BACKGROUND = loadImage("assets/website_background.png")
    FONT_IMAGE = loadImage("assets/font.png")
}

function createGraphicContexts() {
    SCREEN = createGraphics(GAME_WIDTH,GAME_HEIGHT)
    CANVAS = createCanvas(CANVAS_WIDTH,CANVAS_HEIGHT)

    drawingContext.mozImageSmoothingEnabled = false;
    drawingContext.webkitImageSmoothingEnabled = false;
    drawingContext.msImageSmoothingEnabled = false;
    drawingContext.imageSmoothingEnabled = false;

    SCREEN.noSmooth()
}