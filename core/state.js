
let STATE = {
    t: 0,
    money: 0,
    totalMoney: 0,

    serverStates: [],
    serverCost: 10,
    serverMPS: 0.1,

    firewallStates: [],
    firewallCost: 100,

    saleValue: 1,

    monkey_level_values: [0, 5],
    monkey_level_sprites: ['normal', 'angry'],
    monkey_offline_effect: 5 * FPS,
}

function getMillis() {
    return Math.floor(STATE.t / FPS)
}

function sellItems(n) {
    STATE.money += n*STATE.saleValue
    STATE.totalMoney += n*STATE.saleValue
    itemBacklog+=n
}