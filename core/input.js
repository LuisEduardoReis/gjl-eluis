const KEYS = {
    UP: 38,
    DOWN: 40,
    LEFT: 37,
    RIGHT: 39
}

const KEY_STATES = {}

function keyPressed() {
    KEY_STATES[keyCode] = true
}
function keyReleased() {
    KEY_STATES[keyCode] = false
}


// Mouse Input
const MOUSE = {}
const MOUSE_HANDLERS = []
function subscribeMouseInput(handler) {
    MOUSE_HANDLERS.push(handler)
}

function mouseEvent(event) {
    let x = Math.floor(event.layerX * CANVAS_TO_GAME)
    let y = Math.floor(event.layerY * CANVAS_TO_GAME)

    MOUSE_HANDLERS.forEach(handler => {
        if (handler[event.type]) handler[event.type](x,y, event)
    })
}

function mousePressed(event) { 
    MOUSE.pressed = true
    mouseEvent(event) 
}
function mouseReleased(event) { 
    MOUSE.pressed = false
    mouseEvent(event) 
}
function mouseMoved(event) { 
    mouseEvent(event)

    let x = Math.floor(event.layerX * CANVAS_TO_GAME)
    let y = Math.floor(event.layerY * CANVAS_TO_GAME)

    MOUSE.x = x
    MOUSE.y = y
}
