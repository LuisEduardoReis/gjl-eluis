
function createButtons() {
    new SellButton().setPos(8, GAME_HEIGHT - SPRITE_SIZE - 8)

    const s = SPRITE_SIZE
    let x0 = GAME_WIDTH - 4 * s
    let y0 = 2.75 * s
    let dy = 1.25 * s

    new BuyServerButton().setPos(x0, y0)
    new SellServerButton().setPos(x0 - s, y0)

    y0 += dy
    new BuyFirewallButton().setPos(x0, y0)
    new SellFirewallButton().setPos(x0 - s, y0)
}

function drawUI() {
    // Money
    let s = SPRITE_SIZE
    drawText(STATE.money + " E", 2*s, 6, 'center')
    drawText(`${calculateMoneyPerSecond().toFixed(2)} E/S`, 2*s,s+4, 'center')

    // Monkeys
    const monkeysEffPerSec = calculateEffectiveMonkeysPerSecond()

    // Face
    drawSprite(
        SPRITES.monkey[getCurrentMonkeyFace()][0],
        GAME_WIDTH - 4.5 * s - 1,
        8 - 1 * squareWave(monkeysEffPerSec, 0.5)
    )

    // MPS
    drawTextFont(
        `${monkeysEffPerSec.toFixed(2)} M/S`, 
        monkeysEffPerSec > 0 ? FONT_RED : FONT,
        GAME_WIDTH - 3.5 * s, 
        12
    )
    const discount = calculateDiscountPerFirewall() * STATE.firewallStates.length
    if (discount > 0) {
        drawTextFont(
            `-${discount.toFixed(2)} M/S`, 
            FONT_GREEN,
            GAME_WIDTH - 3.5 * s - 4, 
            20
        )
    }

    // INFRASTRUCTURE RATES
    
    if (STATE.firewallStates.length > 0) {
        const pos = getFirewallPosition(0)
        drawTextFont(
            `${calculateDiscountPerFirewall().toFixed(2)} less monkeys per Firewall`, 
            FONT,
            pos.x,
            pos.y + 2*SPRITE_SIZE
        )
    }

    if (STATE.serverStates.length > 0) {
        const pos = getServerPosition(0)
        drawTextFont(
            `${serverMoneyPerSecond().toFixed(2)} E/S per Server`, 
            FONT,
            pos.x,
            pos.y + 2*SPRITE_SIZE
        )
    }
}

