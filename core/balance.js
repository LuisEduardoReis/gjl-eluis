function serverMoneyPerSecond() {
    const totalServers = STATE.serverStates.length
    let moneyPerServer = totalServers == 0 ? 0 : (Math.pow(totalServers, 2) / totalServers)
    moneyPerServer *= STATE.serverMPS

    return moneyPerServer
}

function calculateMoneyPerSecond() {
    let result = 0
    
    
    const activeServers = STATE.serverStates.filter(s => s.offline == 0).length 

    result += activeServers * serverMoneyPerSecond()
    
    return result
}


function calculateMonkeysPerSecond() {
    let rate = 1
    let infraCount = 0
    infraCount += STATE.serverStates.length

    rate /= 10
    rate *= 0.2 * Math.pow(infraCount, 1.15)
    rate *= infraCount

    rate = Math.max(0, rate)
    return rate
}

function calculateDiscountPerFirewall() {
    return Math.pow(STATE.firewallStates.length, 1.17)
}

function calculateEffectiveMonkeysPerSecond() {
    let rate = calculateMonkeysPerSecond()
    
    rate -= calculateDiscountPerFirewall() * STATE.firewallStates.length  

    rate = Math.max(0, rate)
    return rate
}