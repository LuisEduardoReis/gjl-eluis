// Load Assets
function preload() {
    loadImages()
}

STATE.entities = []
function drawEntities() { STATE.entities.forEach(e => e.draw()) }
function updateEntities() { STATE.entities.forEach(e => e.update()) }
function removeDeadEntities() { STATE.entities = STATE.entities.filter(e => !e.remove) }

function setup() {
    frameRate(FPS)
    loadSavedState()

    createGraphicContexts()
    createSprites()
    createFont()

    createButtons()     

    // Test
    //for (let i = 0; i < 10; i++) createServer()
    //for (let i = 0; i < 2; i++) createFirewall()
    //STATE.money = 100000000000000000000
    
}

let saveTimer = 0, savePeriod = 2 * FPS
function draw() {
    // Update
    STATE.t++
    updateInfrastructure()
    updateEntities()
    removeDeadEntities()

    // Save state
    if (saveTimer++ > savePeriod) {
        saveState()
        saveTimer = 0
    }

    // Draw
    SCREEN.image(BACKGROUND, 0,0)
    drawInfrastructure()
    SCREEN.image(WEBSITE_BACKGROUND, 0,2 * SPRITE_SIZE)
    drawEntities()
    drawUI()

    // Draw Buffer
    image(SCREEN,0,0, CANVAS_WIDTH, CANVAS_HEIGHT)    
}