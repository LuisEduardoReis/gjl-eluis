



function createFirewall() {
    STATE.firewallStates.push({
        x: 0, y: 0,
        offline: 0
    })
    updateFirewallPositions()
}

function removeFirewall() {
    STATE.firewallStates.pop()
    updateFirewallPositions()
}

function updateFirewallPositions() {
    STATE.firewallStates.forEach((firewall, i) => {
        const pos = getFirewallPosition(i)
        firewall.x = pos.x
        firewall.y = pos.y
    })
}

function getFirewallPosition(i) {
    const s = SPRITE_SIZE
    let x0 = 4.5 * s
    let y0 = GAME_HEIGHT - 6*s
    let x = x0, y = y0, wn = 10, w = wn * s
    
    if (STATE.firewallStates.length < wn) x += (w / wn) *i
    else x += (w / STATE.firewallStates.length) * i

    return {x: x, y:y}
}

function getFirewallCost(i) {
    return Math.round(Math.pow(STATE.firewallCost, 1 + (i / 8)))
}

function updateFirewalls() {
    STATE.firewallStates.forEach(firewall => {
        firewall.offline = stepTo(firewall.offline, 0)
    })
}

function drawFirewalls() {
    STATE.firewallStates.forEach((firewall) => {
        const fadeOutTime = 5 * FPS
        let blend = 0, x = firewall.x, y = firewall.y

        if (firewall.offline > 0) {
            blend = 1
            if (firewall.offline < fadeOutTime) 
                blend = firewall.offline / fadeOutTime
            
            if (Math.random() > 0.5) x += 1
            if (Math.random() > 0.5) y -= 1
        }

        drawTintedSprite(SPRITES.infra.firewallRed, x,y, blend)
    })
}