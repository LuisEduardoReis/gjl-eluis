const ITEMS = []
let itemBacklog = 0

function queueItem() { 
    let nextItem
    
    do nextItem = randomIntBetween(0,8)
    while (
        nextItem == ITEMS[ITEMS.length - 1] ||
        nextItem == ITEMS[ITEMS.length - 2]
    )

    ITEMS.push(nextItem) 
}
function unqueueItem() { ITEMS.shift() }
function sellItem() { itemBacklog++ }

function updateItems() {
    while(ITEMS.length < 5) queueItem()

    while(itemBacklog >= 1) {
        queueItem()
        unqueueItem()
        itemBacklog--
    }
}

function drawItems() {
    const s = SPRITE_SIZE
    const x0 = 11
    const y0 = 7.5 * s
    ITEMS.forEach((item, i) => {
        drawSprite(
            SPRITES.items[item], 
            x0, 
            y0 - i * s
        )
    })
}

