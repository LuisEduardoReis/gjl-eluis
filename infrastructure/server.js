



function createServer() {
    STATE.serverStates.push({
        x: 0, y: 0,
        offline: 0
    })
    updateServerPositions()
}

function removeServer() {
    STATE.serverStates.pop()
    updateServerPositions()
}

function updateServerPositions() {
    STATE.serverStates.forEach((server, i) => {
        const pos = getServerPosition(i)
        server.x = pos.x
        server.y = pos.y
    })
}

function getServerPosition(i) {
    const s = SPRITE_SIZE
    let x0 = 4.5 * s
    let y0 = GAME_HEIGHT - 3*s
    let x = x0, y = y0, wn = 10, w = wn * s
    
    if (STATE.serverStates.length < wn) x += (w / wn) *i
    else x += (w / STATE.serverStates.length) * i

    return {x: x, y:y}
}

function getServerCost(i) {
    return Math.round(Math.pow(STATE.serverCost, 1 + (i / 8)))
}

function updateServers() {
    STATE.serverStates.forEach(server => {
        server.offline = stepTo(server.offline, 0)
    })
}

function drawServers() {
    STATE.serverStates.forEach((server) => {
        const fadeOutTime = 5 * FPS
        let blend = 0, x = server.x, y = server.y

        if (server.offline > 0) {
            blend = 1
            if (server.offline < fadeOutTime) 
                blend = server.offline / fadeOutTime
            
            if (Math.random() > 0.5) x += 1
            if (Math.random() > 0.5) y -= 1
        }

        drawTintedSprite(SPRITES.infra.serverRed, x,y, blend)
    })
}