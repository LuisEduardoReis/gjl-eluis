
let MONEY_ACCUM = 0

function updateInfrastructure() {
    // Increase Money
    MONEY_ACCUM += calculateMoneyPerSecond() / FPS
    if (MONEY_ACCUM >= 1) {
        const addedMoney = Math.floor(MONEY_ACCUM)
        sellItems(addedMoney)
        MONEY_ACCUM -= addedMoney
    }

    // Update Infrastructures
    updateMonkeys()
    updateServers()
    updateFirewalls()
    updateItems()
    
}

function drawInfrastructure() {
    drawServers()
    drawFirewalls()
    drawItems()
}