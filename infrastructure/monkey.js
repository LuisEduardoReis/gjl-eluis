
function rollMonkeys() {
    const rate = calculateEffectiveMonkeysPerSecond() / FPS
    const span = Math.ceil(rate + Number.EPSILON)
    const ratePerRoll = rate / span
    
    let monkeys = 0
    for (let i = 0; i < span; i++) {
        if (Math.random() < ratePerRoll) monkeys++
    }

    return monkeys
}

function updateMonkeys() {
    let victims = []
    victims = victims.concat(STATE.serverStates)

    let monkeys = rollMonkeys()

    while(victims.length > 0 && monkeys > 0) {
        const victimIndex = randomIntBetween(0, victims.length)
        const victim = victims.splice(victimIndex, 1)[0]

        victim.offline = STATE.monkey_offline_effect
        monkeys--

        if (STATE.entities.length < MAX_ENTITIES) createMonkeyEntity(victim)
    }
}

function createMonkeyEntity(victim) {
    new Monkey().setPos(victim.x, victim.y)
}

function getCurrentMonkeyFace() {
    const monkeysPerSec = calculateMonkeysPerSecond()
    let monkeyLevel = 0
    while(true) {
        if (monkeyLevel >= STATE.monkey_level_values.length-1) break
        if (monkeysPerSec < STATE.monkey_level_values[monkeyLevel+1]) break
        monkeyLevel++
    }
    return STATE.monkey_level_sprites[monkeyLevel]
}