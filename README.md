# GJL - eLuis

## Context
This was a game made in 48h for the self hosted game jam **"GJM - Game Jam do Luis"**

The theme for the jam, decided by chat, was **Chaos Monkey**

## About the Game
It's an idle game in which the objective is to make the sales website *eLuis* as successful as possible while keeping its infrastructure safe from chaos monkey attacks.

## Play it!
The game can be played at **https://luisreis.net/gjl/**  
You can also play the jam version at **https://luisreis.net/gjl/jam/**

![monkey](assets/favicon.png) ![monkey](assets/favicon.png) ![monkey](assets/favicon.png)

![GJL Flyer](assets/flyer.png)


