BASEDIR=$(dirname "$0")
SRC=$BASEDIR
SECRETS=`cat $BASEDIR/rsync-secrets.json`
REMOTE_BASEDIR=$(echo $SECRETS | jq -r '.remote_basedir')
REMOTE_HOST=$(echo $SECRETS | jq -r '.remote_host')
PEM_FILE_PATH=$(echo $SECRETS | jq -r '.pem_file_path')

echo "Remote Basedir: '$REMOTE_BASEDIR'"
echo "Remote Host: '$REMOTE_HOST'"
echo "Pem File Path: '$PEM_FILE_PATH'"

DEST="${REMOTE_BASEDIR}$1"
DEST_HOST="${REMOTE_HOST}"
SSH_CMD="ssh -i $PEM_FILE_PATH"

sudo rsync --exclude=".*" -vr -e "$SSH_CMD" $SRC $DEST_HOST:$DEST
