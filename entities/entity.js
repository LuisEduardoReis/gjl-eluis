class Entity {

    constructor() {
        STATE.entities.push(this)

        this.x = 0
        this.y = 0
        this.w = SPRITE_SIZE
        this.h = SPRITE_SIZE

        this.dx = 0
        this.dy = 0

        this.ttl = -1

        this.remove = false
    }

    setPos(x,y) {
        this.x = Math.floor(x)
        this.y = Math.floor(y)
        return this
    }

    update() {
        this.x += this.dx
        this.y += this.dy

        if (this.ttl > 0) this.ttl = stepTo(this.ttl, 0)
        if (this.ttl == 0) this.remove = true
    }

    draw() {
        if (this.sprite) drawSprite(this.sprite, this.x, this.y)
    }
}