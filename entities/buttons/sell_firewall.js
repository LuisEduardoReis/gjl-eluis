
class SellFirewallButton extends Button {

    constructor() {
        super(SPRITES.upgrades.firewallUp, SPRITE_SIZE, SPRITE_SIZE)

        this.upSprite = SPRITES.upgrades.sellUp
        this.downSprite = SPRITES.upgrades.sellDown
        this.overSprite = SPRITES.upgrades.sellOver

        this.border = 2
    }

    update() {
        this.enabled = STATE.firewallStates.length > 0
    }

    up() {
        if (this.enabled) {
            STATE.money += getFirewallCost(STATE.firewallStates.length - 1)
            removeFirewall()
        }
    }
    draw() {
        if (this.enabled) super.draw()
    }
}