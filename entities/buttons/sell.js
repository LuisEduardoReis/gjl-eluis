let SELL_SPRITE = null


class SellButton extends Button {

    constructor() {
        super(SPRITES.sell.up, 3*SPRITE_SIZE, SPRITE_SIZE)

        this.upSprite = SPRITES.sell.up
        this.downSprite = SPRITES.sell.down
        this.overSprite = SPRITES.sell.over

        this.w = 3 * SPRITE_SIZE
        this.h = 1 * SPRITE_SIZE
    }

    up() {
        sellItems(1)
        sellItem()
    }
}