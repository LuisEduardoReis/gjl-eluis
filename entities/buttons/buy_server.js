
class BuyServerButton extends Button {

    constructor() {
        super(SPRITES.upgrades.serverUp, SPRITE_SIZE, SPRITE_SIZE)

        this.upSprite = SPRITES.upgrades.serverUp
        this.downSprite = SPRITES.upgrades.serverDown
        this.overSprite = SPRITES.upgrades.serverOver
    }

    update() {
        this.enabled = STATE.money >= getServerCost(STATE.serverStates.length)
    }

    up() {
        if (STATE.money >= getServerCost(STATE.serverStates.length)) {
            STATE.money -= getServerCost(STATE.serverStates.length)
            createServer()
        }
    }

    draw() {
        super.draw()
        const s = SPRITE_SIZE
        if (STATE.serverStates.length > 0) {
            drawText(
                STATE.serverStates.length, 
                this.x + 8, 
                this.y + 12,
                'center'
            )
        }
        
        drawText(
            getServerCost(STATE.serverStates.length) + " M", 
            this.x + 18, 
            this.y + 6
        )
    }
}