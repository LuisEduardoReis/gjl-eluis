class Button extends Entity {
    constructor(sprite, w,h) {
        super()

        this.upSprite = sprite
        this.downSprite = sprite
        this.overSprite = sprite

        this.upTint = 'white'
        this.downTint = 'white'
        this.overTint = 'white'

        this.w = w
        this.h = h

        this.enabled = true
        this.border = 0

        subscribeMouseInput(this)
    }

    update() {
    }

    draw() {
        let sprite = this.upSprite

        if (this.enabled && this.isMouseOver() && MOUSE.pressed)
            sprite = this.downSprite
        
        if (this.enabled && this.isMouseOver() && !MOUSE.pressed)
            sprite = this.overSprite
        
        if (!this.enabled)
            sprite = this.downSprite

        drawSprite(eval(sprite), this.x, this.y)
    }

    up() {}
    down() {}
    over() {}

    isMouseOver(x,y, override) {
        if (!override) {
            x = MOUSE.x
            y = MOUSE.y
        }
        const b = this.border
        return inside(x, y, this.x + b, this.y + b, this.w - 2*b, this.h - 2*b)
    }

    mousedown(x,y) {
        if (this.isMouseOver(x,y, true)) this.down()
    }

    mouseup(x,y) {
        if (this.isMouseOver(x,y, true)) this.up()
    }
}