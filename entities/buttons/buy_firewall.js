
class BuyFirewallButton extends Button {

    constructor() {
        super(SPRITES.upgrades.firewallUp, SPRITE_SIZE, SPRITE_SIZE)

        this.upSprite = SPRITES.upgrades.firewallUp
        this.downSprite = SPRITES.upgrades.firewallDown
        this.overSprite = SPRITES.upgrades.firewallOver
    }

    update() {
        this.enabled = STATE.money >= getFirewallCost(STATE.firewallStates.length)
    }

    up() {
        if (STATE.money >= getFirewallCost(STATE.firewallStates.length)) {
            STATE.money -= getFirewallCost(STATE.firewallStates.length)
            createFirewall()
        }
    }

    draw() {
        super.draw()
        const s = SPRITE_SIZE
        if (STATE.firewallStates.length > 0) {
            drawText(
                STATE.firewallStates.length, 
                this.x + 8, 
                this.y + 12,
                'center'
            )
        }
        
        drawText(
            getFirewallCost(STATE.firewallStates.length) + " M", 
            this.x + 18, 
            this.y + 6
        )
    }
}