
class SellServerButton extends Button {

    constructor() {
        super(SPRITES.upgrades.serverUp, SPRITE_SIZE, SPRITE_SIZE)

        this.upSprite = SPRITES.upgrades.sellUp
        this.downSprite = SPRITES.upgrades.sellDown
        this.overSprite = SPRITES.upgrades.sellOver

        this.border = 2
    }

    update() {
        this.enabled = STATE.serverStates.length > 0
    }

    up() {
        if (this.enabled) {
            STATE.money += getServerCost(STATE.serverStates.length - 1)
            removeServer()
        }
    }
    draw() {
        if (this.enabled) super.draw()
    }
}