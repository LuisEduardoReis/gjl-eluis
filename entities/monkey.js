
class Monkey extends Entity {

    constructor() {
        super()

        this.ttl = FPS

        this.sprite = SPRITES.monkey[getCurrentMonkeyFace()]

        this.dy = - SPRITE_SIZE / (1 * FPS) // 1 Tile / Sec
    }

    update() {
        super.update()
    }

    draw() {
        let alpha = 1
        if (this.ttl >= 0 && this.ttl < FPS) {
            alpha = this.ttl / FPS
        }
        drawTintedSprite(this.sprite, this.x,this.y, 1 - alpha)
    }
}