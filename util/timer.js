class Timer {

    constructor() {
        this.t = 0
        this.duration = 0
        this.speed = 1
    }

    update() {
        this.t += this.speed
    }

    
}